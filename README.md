# What's here
Repository dove provo a seguire il [tutorial di django](https://docs.djangoproject.com/en/4.2/intro/tutorial01/) (e seguenti)



# Anaconda env
```
conda create --name webapp
pip install -r requirements.txt
```
where [requirements.txt](https://github.com/sclorg/django-ex/blob/master/requirements.txt)
and then `pip install django --upgrade`because of some test problems with older version



# To run server
`python manage.py runserver`

# Dump of some commands
```
 django-admin startproject mysite

 1980  python manage.py runserver
 1984  python manage.py startapp polls
 1985  python manage.py runserver
 2005  python manage.py sqlmigrate polls 0001
 2008  python manage.py check
 2009  python manage.py migrate
 2011  python manage.py shell
 2012  python manage.py createsuperuser
 2013  python manage.py runserver
 2019  python manage.py runserver
```



# Performing test
`python manage.py test polls`  
For lecture 8 `pip install django-debug-toolbar`

